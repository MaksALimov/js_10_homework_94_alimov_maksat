const mongoose = require('mongoose');

const EventSchema = new mongoose.Schema({
    date: {
        type: Date,
        required: true,
        validate: {
            validator: value => {
                return (
                    value && value.getTime() >= Date.now() - 24 * 60 * 60 * 1000
                )
            },
            message: 'You cannot plan an event to the past.'
        }
    },

    description: {
        type: String,
        required: true,
    },

    duration: {
        type: Number,
        required: true,
        min: [0.1],
    },

    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },

    datetime: {
        type: Date,
        default: Date.now,
    },

    sharedEvents: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
        }
    ],
});

const Event = mongoose.model('Event', EventSchema);

module.exports = Event;