require('dotenv').config();
const express = require('express');
const app = express();
const cors = require('cors');
const exitHook = require('async-exit-hook');
const mongoose = require('mongoose');
const config = require('./config');
const users = require('./routes/users');
const events = require('./routes/events');
const port = 8000;

app.use(cors());
app.use(express.json());
app.use('/users', users);
app.use('/events', events);

const run = async () => {
    await mongoose.connect(config.db.url);

    app.listen(port, () => {
       console.log(`Server started on ${port} port!`);
    });

    exitHook(() => {
       console.log('Exiting');
       mongoose.disconnect();
    });
};

run().catch(e => console.error(e));