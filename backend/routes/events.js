const express = require('express');
const router = express.Router();
const User = require('../models/User');
const Event = require('../models/Event');
const auth = require('../middleware/auth');

router.get('/', auth, async (req, res) => {
    try {
        const userId = {};

        if (req.query.userId) {
            userId.userId = req.query.userId;
            const userEvents = await Event.find(userId).sort({date: 1});

            if (userEvents.length === 0) {
                return res.send(userEvents);
            }

            if (userEvents[0].sharedEvents.length === 0) {
                return res.send(userEvents);
            }

            const sharedUserId = [];

            for (let i = 0; i < userEvents.length; i++) {
                sharedUserId.push(userEvents[i].sharedEvents);
            }

            if (!sharedUserId) {
                return res.send(userEvents);
            }

            const sharedUserEvents = await Event.find({userId: sharedUserId.flat(1)});

            const filteredSharedEvents = sharedUserEvents.filter(user => user.sharedEvents.length > 0);

            for (let i = 0; i < filteredSharedEvents.length; i++) {
                const users = await Event.find({userId: filteredSharedEvents[i].sharedEvents});
                sharedUserEvents.push(users[i]);
            }

            const united = [...userEvents, ...sharedUserEvents];

            const filtered = Object.values(united.reduce((acc, cur) => Object.assign(acc, {[cur.id]: cur}), {}));
            // удалил дубликаты из united
            res.send(filtered);
        }

    } catch (error) {
        res.sendStatus(500);
    }
});

router.post('/', auth, async (req, res) => {
    try {
        const eventData = {
            date: req.body.date,
            description: req.body.description,
            duration: req.body.duration,
            userId: req.body.userId,
        };

        const event = new Event(eventData);

        await event.save();
        res.send(event);

    } catch (error) {
        res.status(400).send(error);
    }
});

router.post('/:email', auth, async (req, res) => {
    try {
        const user = await User.findOne({email: req.params.email});

        if (!user) {
            return res.status(404).send({message: 'User Not Found'});
        }

        const events = await Event.findOne({userId: user._id});

        if (!events) {
            return res.status(403).send({message: user.displayName + ' must have events'});
        }

        for (let i = 0; i < events.sharedEvents.length; i++) {
            if (String(events.sharedEvents[i]) === String(user._id)) {
                return res.send({warning: 'You cannot share with your event with someone twice'});
            }
        }
        events.sharedEvents.push(req.body);
        await events.save({validateBeforeSave: false});
        await events.save();

        res.send(events);
    } catch (error) {
        return res.sendStatus(500);
    }
});

router.delete('/:eventId', async (req, res) => {
    try {
        const event = await Event.findByIdAndDelete(req.params.eventId);

        if (!event) {
            return res.status(404).send({error: 'Event Not Found'});
        }

        return res.send(event);

    } catch (error) {
        res.sendStatus(500);
    }
});

module.exports = router;