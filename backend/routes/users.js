const express = require('express');
const router = express.Router();
const User = require('../models/User');
const {nanoid} = require("nanoid");
const config = require('../config');
const {OAuth2Client} = require("google-auth-library");
const clientGoogle = new OAuth2Client(config.google.clientId);

router.post('/', async (req, res) => {
    try {
        const userData = {
            email: req.body.email,
            password: req.body.password,
            displayName: req.body.displayName,
        };

        const user = new User(userData);
        user.generateToken();
        await user.save();
        res.send(user);

    } catch (error) {
        res.status(400).send(error);
    }
});


router.post('/sessions', async (req, res) => {
    try {
        const user = await User.findOne({email: req.body.email});

        if (!user) {
            return res.status(401).send({message: 'Data not valid'});
        }

        const isMatch = await user.checkPassword(req.body.password);

        if (!isMatch) {
            return res.status(401).send({message: 'Data not valid'});
        }

        user.generateToken();
        await user.save({validateBeforeSave: false});
        res.send(user);
    } catch (error) {
        return res.sendStatus(500);
    }
});

router.post('/googleLogin', async (req, res) => {
    try {
        const ticket = await clientGoogle.verifyIdToken({
            idToken: req.body.tokenId,
            audience: config.google.clientId,
        });

        const {name, email, sub: ticketUserId} = ticket.getPayload();

        if (ticketUserId !== req.body.googleId) {
            return res.status(401).send({global: 'User Id incorrect'});
        }

        let user = await User.findOne({email});

        if (!user) {
            user = new User({
                email,
                password: nanoid(),
                displayName: name,
            })
        }

        user.generateToken();
        await user.save({validateBeforeSave: false});
        res.send(user);
    } catch (error) {
        res.status(500).send({global: 'Server error. Try again'});
    }
});


router.delete('/sessions', async (req, res) => {
    try {
        const token = req.get('Authorization');
        const success = {message: 'Success'};

        if (!token) return res.send(success);

        const user = await User.findOne({token});

        if (!user) return res.send(success);

        user.generateToken();

        await user.save({validateBeforeSave: false});

        return res.send(success);
    } catch (error) {
        return res.sendStatus(500)
    }
});
module.exports = router;