const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require('./models/User');
const Event = require('./models/Event');

const run = async () => {
    await mongoose.connect(config.db.url);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [firstUser, secondUser] = await User.create({
        email: 'admin@gmail.com',
        password: '123',
        token: nanoid(),
        displayName: 'Admin'
    }, {
        email: 'user@gmail.com',
        password: '123',
        token: nanoid(),
        displayName: 'User',
    });

    await Event.create({
        date: '2021-12-13',
        description: 'First event of first user',
        duration: '2',
        userId: firstUser,
        datetime: new Date("12/13/2021 13:45"),
        sharedEvents: secondUser
    }, {
        date: '2021-12-13',
        description: 'Second event of second user',
        duration: '4',
        userId: secondUser,
        datetime: new Date("12/13/2021 12:45"),
    });

    await mongoose.connection.close();
};

run().catch(e => console.error(e));
