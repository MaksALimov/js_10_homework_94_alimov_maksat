module.exports = {
    db: {
        url: 'mongodb://localhost/calendar-events'
    },

    google: {
      clientId: process.env.GOOGLE_CLIENT_ID,
    }
}