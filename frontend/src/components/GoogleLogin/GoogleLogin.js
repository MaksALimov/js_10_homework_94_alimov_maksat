import React from 'react';
import GoogleLoginButton from 'react-google-login';
import {googleClientId} from '../../config'
import {Button} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {googleLogin} from "../../store/actions/usersActions";

const GoogleLogin = () => {
    const dispatch = useDispatch();

    const googleLoginHandler = response => {
        dispatch(googleLogin(response));
    };

    return (
        <GoogleLoginButton
            clientId={googleClientId}
            render={props => (
                <Button fullWidth variant="outlined" color="primary" onClick={props.onClick}>
                    Login with Google
                </Button>
            )}
            onSuccess={googleLoginHandler}
            cookiePolicy={'single_host_origin'}
        />
    );
};

export default GoogleLogin;