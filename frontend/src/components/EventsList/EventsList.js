import React from 'react';
import dayjs from "dayjs";
import {Grid, makeStyles, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {deleteEventRequest} from "../../store/actions/eventsActions";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles(() => ({
    event: {
        margin: '20px 0',
        boxShadow: 'rgba(0, 0, 0, 0.35) 0px 5px 15px',
        padding: '20px',
        '&:first-child': {
            marginTop: '0',
        },
        borderRadius: '15px',
    },

    dateEvent: {
        fontSize: '33px',
        fontWeight: 'bold',
    },

    eventDescription: {
      fontSize: '23px',
    },

    eventDuration: {
      fontSize: '23px',
    },

    deleteEventBtn: {
      margin: '20px 0',
    },
}));

const EventsList = ({events}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const loading = useSelector(state => state.events.deleteEventLoading);

    return (
        <>
            {events.map(event => (
                <Grid key={event._id} className={classes.event}>
                    <Typography className={classes.dateEvent}>
                        Event Date: {dayjs(event.date).format('YYYY-MM-DD')}
                    </Typography>
                    <Typography className={classes.eventDescription}>
                        Event Description: {event.description}
                    </Typography>
                    <Typography className={classes.eventDuration}>
                        Event Duration: {event.duration}
                    </Typography>
                    {user._id === event.userId ? (
                        <ButtonWithProgress
                            variant="contained"
                            className={classes.deleteEventBtn}
                            loading={loading}
                            disabled={loading}
                            onClick={() => dispatch(deleteEventRequest(event._id))}
                        >
                            Delete Event
                        </ButtonWithProgress>
                    ) : null}
                </Grid>
            ))}
        </>
    );
};

export default EventsList;