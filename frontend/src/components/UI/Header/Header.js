import React from 'react';
import {useSelector} from "react-redux";
import {Link as RouterLink} from "react-router-dom";
import Anonymous from "./Menu/Anonymous";
import UserMenu from "./Menu/UserMenu";
import {AppBar, Grid, makeStyles, Toolbar, Typography} from "@material-ui/core";

const useStyles = makeStyles(() => ({
    container: {
        padding: '20px',
    },

    mainLink: {
        color: 'inherit',
        textDecoration: 'none',
        '&:hover': {
            color: 'inherit',
        },
    },
}));

const Header = () => {
    const classes = useStyles();
    const user = useSelector(state => state.users.user);

    return (
        <>
            <AppBar position="fixed">
                <Toolbar>
                    <Grid container
                          justifyContent="space-between"
                          alignItems="center"
                          className={classes.container}
                    >
                        <Grid item>
                            <Typography variant="h3">
                                <RouterLink to="/" className={classes.mainLink}>
                                    Calendar Events
                                </RouterLink>
                            </Typography>
                        </Grid>
                        <Grid item>
                            {user ? <UserMenu/> : <Anonymous/>}
                        </Grid>
                    </Grid>
                </Toolbar>
            </AppBar>
        </>
    );
};

export default Header;