import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Button, Grid, makeStyles, Typography} from "@material-ui/core";
import {logoutUserRequest} from "../../../../store/actions/usersActions";

const useStyles = makeStyles(() => ({
    displayName: {
      fontSize: '35px',
    },
}));

const UserMenu = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);

    return (
        <Grid container spacing={6} alignItems="center">
            <Grid item>
                <Typography className={classes.displayName}>
                    Hello {user.displayName}
                </Typography>
            </Grid>
            <Grid item>
                <Button variant="contained" onClick={() => dispatch(logoutUserRequest())}>
                    Logout
                </Button>
            </Grid>
        </Grid>
    );
};

export default UserMenu;