import React from 'react';
import {Grid, TextField} from "@material-ui/core";
import PropTypes from "prop-types";

const FormElement = ({label, name, value, onChange, required, error, autoComplete, type, multiline, rows}) => {

    return (
        <Grid item xs={12}>
            <TextField
                multiline={multiline}
                rows={rows}
                autoComplete={autoComplete}
                required={required}
                type={type}
                fullWidth
                variant="outlined"
                label={label}
                name={name}
                value={value}
                onChange={onChange}
                error={Boolean(error)}
                helperText={error}
            />
        </Grid>
    );
};

FormElement.propTypes = {
    name: PropTypes.string.isRequired,
    value: PropTypes.any.isRequired,
    onChange: PropTypes.func.isRequired,
    required: PropTypes.bool,
    error: PropTypes.string,
    autoComplete: PropTypes.string,
    type: PropTypes.string,
    multiline: PropTypes.bool,
    rows: PropTypes.number,
};
export default FormElement;