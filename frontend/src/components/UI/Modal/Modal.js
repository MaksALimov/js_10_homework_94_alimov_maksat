import React, {useState} from 'react';
import {Button, Grid, makeStyles, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import Backdrop from "../BackDrop/BackDrop";
import {inviteUserRequest, setModal} from "../../../store/actions/eventsActions";
import FormElement from "../Form/FormElement";

const useStyles = makeStyles(() => ({
    modal: {
        position: 'fixed',
        zIndex: '500',
        backgroundColor: 'white',
        width: '70%',
        border: '1px solid #ccc',
        boxShadow: '1px 1px 1px black',
        padding: '16px',
        left: '15%',
        top: '30%',
        boxSizing: 'border-box',
        transition: 'all 0.3s ease-out',
    },

    title: {
        fontSize: '28px',
        margin: '10px 0',
    },

    buttons: {
        margin: '10px 0',
    },
}));

const Modal = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const [email, setEmail] = useState('');

    const shareEvents = () => {
        dispatch(inviteUserRequest({user, email}));
        dispatch(setModal(false))
    };

    return (
        <>
            <Backdrop/>
            <Grid container direction="column" className={classes.modal} alignItems="center">
                <Grid item>
                    <Typography component="h3" className={classes.title}>Share events with another user</Typography>
                </Grid>
                <Grid item component="form">
                    <FormElement
                        variant="outlined"
                        label="Email"
                        value={email}
                        name="email"
                        onChange={e => setEmail(e.target.value)}
                    />
                </Grid>
                <Button
                    variant="contained"
                    onClick={() => dispatch(setModal(false))}
                    className={classes.buttons}
                >
                    Close
                </Button>
                <Button
                    variant="contained"
                    className={classes.buttons}
                    onClick={shareEvents}
                >
                    Invite
                </Button>
            </Grid>
        </>
    );
};

export default Modal;