import React, {useState} from 'react';
import {Container, Grid, makeStyles} from "@material-ui/core";
import FormElement from "../../components/UI/Form/FormElement";
import {useDispatch, useSelector} from "react-redux";
import {addEventRequest} from "../../store/actions/eventsActions";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles(() => ({
    form: {
        marginTop: '200px',
    },
}));

const AddEvent = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const error = useSelector(state => state.events.eventsError);
    const loading = useSelector(state => state.events.eventsLoading);

    const [state, setState] = useState({
        description: "",
        date: "",
        duration: "",
    });

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const submitFormHandler = e => {
        e.preventDefault();
        dispatch(addEventRequest({...state, userId: user._id}))
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <Container maxWidth="xs">
            <Grid container
                  component="form"
                  className={classes.form}
                  spacing={2}
                  onSubmit={submitFormHandler}
                  justifyContent="center"
            >
                <FormElement
                    onChange={inputChangeHandler}
                    type="date"
                    value={state.date}
                    name="date"
                    error={getFieldError('date')}
                />
                <FormElement
                    onChange={inputChangeHandler}
                    type="text"
                    label="Description of Event"
                    multiline
                    rows={3}
                    value={state.description}
                    name="description"
                    error={getFieldError('description')}
                />
                <FormElement
                    onChange={inputChangeHandler}
                    type="number"
                    label="Duration of Event"
                    value={state.duration}
                    name="duration"
                    error={getFieldError('duration')}
                />

                <Grid item>
                    <ButtonWithProgress
                        type="submit"
                        variant="contained"
                        loading={loading}
                        disabled={loading}
                    >
                        Submit
                    </ButtonWithProgress>
                </Grid>
            </Grid>
        </Container>
    );
};

export default AddEvent;