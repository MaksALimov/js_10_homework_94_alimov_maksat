import React, {useEffect} from 'react';
import {Grid, makeStyles} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {inviteUsersRequest} from "../../store/actions/inviteActions";

const useStyles = makeStyles(() => ({
    container: {
        marginTop: '150px',
    },
}));

const Invited = () => {
    const classes = useStyles();
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(inviteUsersRequest());
    }, [dispatch]);

    return (
        <Grid className={classes.container}>
            Invited
        </Grid>
    );
};

export default Invited;