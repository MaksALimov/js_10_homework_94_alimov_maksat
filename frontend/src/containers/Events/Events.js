import React, {useEffect} from 'react';
import {Button, CircularProgress, Grid, makeStyles} from "@material-ui/core";
import EventsList from "../../components/EventsList/EventsList";
import {Link as RouterLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {getEventRequest, setModal} from "../../store/actions/eventsActions";
import Modal from "../../components/UI/Modal/Modal";

const useStyles = makeStyles(() => ({
    container: {
        marginTop: '150px',
    },

    progressContainer: {
        display: 'flex',
        justifyContent: 'center',
        marginTop: '250px',
    },

    btnLinks: {
        textDecoration: 'none',
        color: 'inherit',
        fontWeight: 'bold',
        fontSize: '20px',
    },

    eventsContainer: {
        flexBasis: '800px',
    },
}));

const Events = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const events = useSelector(state => state.events.events);
    const loading = useSelector(state => state.events.getEventsLoading);
    const modal = useSelector(state => state.events.setModal);

    useEffect(() => {
        dispatch(getEventRequest(user._id));
    }, [dispatch, user._id]);

    return (
        <>
            {loading ? <Grid className={classes.progressContainer}><CircularProgress/></Grid> : (
                <Grid
                    container
                    justifyContent="space-around"
                    className={classes.container}
                    wrap="nowrap"
                >
                    <Grid item className={classes.eventsContainer}>
                        <EventsList events={events}/>
                    </Grid>
                    <Grid>
                        <Button variant="contained">
                            <RouterLink to="/add-event" className={classes.btnLinks}>
                                Add
                            </RouterLink>
                        </Button>
                    </Grid>
                    {events.length > 0 ? (
                        <Grid>
                            <Button
                                variant="contained"
                                className={classes.btnLinks}
                                onClick={() => dispatch(setModal(true))}
                            >
                                Invite
                            </Button>
                        </Grid>
                    ): null}
                    <Grid>
                        <Button variant="contained">
                            <RouterLink to="/invited" className={classes.btnLinks}>
                                View Invited
                            </RouterLink>
                        </Button>
                    </Grid>
                </Grid>
            )}
            {modal ? <Modal/> : null}
        </>
    );
};

export default Events;