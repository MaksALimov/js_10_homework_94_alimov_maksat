import {combineReducers} from "redux";
import {configureStore} from "@reduxjs/toolkit";
import createSagaMiddleware from 'redux-saga';
import {rootSagas} from "./rootSagas";
import usersSlice, {initialState} from "./slices/usersSlice";
import axiosApi from "../axiosApi";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import eventsSlice from "./slices/eventsSlice";

const rootReducer = combineReducers({
    users: usersSlice.reducer,
    events: eventsSlice.reducer,
});

const persistedState = loadFromLocalStorage();

const sagaMiddleware = createSagaMiddleware();

const store = configureStore({
    reducer: rootReducer,
    middleware: [sagaMiddleware],
    devTools: true,
    preloadedState: persistedState
});

sagaMiddleware.run(rootSagas);

store.subscribe(() => {
    saveToLocalStorage({
        users: {
            ...initialState,
            user: store.getState().users.user,
        }
    });
});

axiosApi.interceptors.request.use(config => {
    try {
        config.headers['Authorization'] = store.getState().users.user.token
    } catch (e) {}

    return config;
});

axiosApi.interceptors.response.use(res => res, e => {
    if (!e.response) {
        e.response = {data: {global: 'No internet'}};
    }

    throw e;
});

export default store;