import {takeEvery, put} from 'redux-saga/effects';
import {
    googleLogin,
    loginUserFailure, loginUserRequest,
    loginUserSuccess, logoutUserFailure, logoutUserRequest,
    registerUserFailure,
    registerUserRequest,
    registerUserSuccess
} from "../actions/usersActions";
import {toast} from "react-toastify";
import axiosApi from "../../axiosApi";
import {historyPush} from "../actions/historyActions";

function* registerUserSaga({payload: userData}) {
    try {
        const response = yield axiosApi.post('/users', userData);
        yield put(registerUserSuccess(response.data));
        toast.success('Register successful!');
        historyPush('/');
    } catch (error) {
        yield put(registerUserFailure(error.response.data));
        toast.error(error.response.data.global);
    }
}

function* loginUserSaga({payload: userData}) {
    try {
        const response = yield axiosApi.post('/users/sessions', userData);
        yield put(loginUserSuccess(response.data));
        toast.success('Login successful!');
        historyPush('/');
    } catch (error) {
        yield put(loginUserFailure(error.response.data));
        toast.error(error.response.data.global);
    }
}

function* loginGoogle({payload: googleData}) {
    try {
        const response = yield axiosApi.post('/users/googleLogin', {
            tokenId: googleData.tokenId,
            googleId: googleData.googleId,
        });
        yield put(loginUserSuccess(response.data));
        toast.success('Login successful!');
        historyPush('/');
    } catch (error) {
        yield put(loginUserFailure(error.response.data));
        toast.error(error.response.data.global);
    }
}

function* logoutUser() {
    try {
        yield axiosApi.delete('/users/sessions');
    } catch (error) {
        yield put(logoutUserFailure(error.response.data));
        toast.error(error.response.data.global);
    }
}

const usersSaga = [
    takeEvery(registerUserRequest, registerUserSaga),
    takeEvery(loginUserRequest, loginUserSaga),
    takeEvery(logoutUserRequest, logoutUser),
    takeEvery(googleLogin, loginGoogle),
];

export default usersSaga;