import {put, takeEvery} from 'redux-saga/effects';
import {
    addEventFailure,
    addEventRequest,
    addEventSuccess,
    deleteEventFailure,
    deleteEventRequest,
    deleteEventSuccess,
    getEventFailure,
    getEventRequest,
    getEventSuccess, inviteUserFailure, inviteUserRequest, inviteUserSuccess
} from "../actions/eventsActions";
import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {historyPush} from "../actions/historyActions";

function* addEvent({payload: eventData}) {
    try {
        const response = yield axiosApi.post('/events', eventData);
        yield put(addEventSuccess(response.data));
        toast.success('Event was added');
        historyPush('/');
    } catch (error) {
        yield put(addEventFailure(error.response.data));
        toast.error(error.response.data);
    }
}

function* getUserEvents({payload: userId}) {
    try {
        const response = yield axiosApi.get(`/events?userId=${userId}`);
        yield put(getEventSuccess(response.data));
    } catch (error) {
        yield put(getEventFailure(error.response.data));
        toast.error(error.response.data);
    }
}

function* deleteUserEvent({payload: eventId}) {
    try {
        yield axiosApi.delete(`/events/${eventId}`);
        yield put(deleteEventSuccess(eventId));
        toast.success('You successfully deleted event');
    } catch (error) {
        yield put(deleteEventFailure(error.response.data));
        if (!error.response.data.global) {
            toast.error(error.response.data);
        } else {
            toast.error(error.response.data.global);
        }
    }
}

function* inviteUser({payload}) {
    try {
        yield axiosApi.post(`/events/${payload.email}`, payload.user);
        yield put(inviteUserSuccess());
        toast.success(`You shared with your posts with ${payload.email}`);
    } catch (error) {
        yield put(inviteUserFailure(error.response.data));
        if (!error.response.data.global) {
            toast.error(error.response.data.message);
            if (!error.response.data.message) {
                toast.error(error.response.data);
            }
        } else {
            console.log(error);
            toast.error(error.response.data.global);
        }
    }
}

export const eventsSaga = [
    takeEvery(addEventRequest, addEvent),
    takeEvery(getEventRequest, getUserEvents),
    takeEvery(deleteEventRequest, deleteUserEvent),
    takeEvery(inviteUserRequest, inviteUser),
];