import {createSlice} from '@reduxjs/toolkit';

export const initialState = {
    events: [],
    eventsRequest: null,
    eventsLoading: false,
    eventsError: null,
    getEventsLoading: false,
    getEventsError: null,
    deleteEventLoading: false,
    deleteEventError: null,
    inviteUserLoading: false,
    inviteUserError: null,
    setModal: false,
};

const eventSlice = createSlice({
    name: 'events',
    initialState,

    reducers: {
        addEventRequest(state) {
            state.eventsLoading = true;
        },

        addEventSuccess(state, action) {
            state.events = [...state.events, action.payload];
            state.eventsLoading = false;
        },

        addEventFailure(state, action) {
            state.eventsError = action.payload;
            state.eventsLoading = false;
        },

        getEventRequest(state) {
            state.getEventsLoading = true;
        },

        getEventSuccess(state, action) {
            state.getEventsLoading = false;
            state.events = action.payload;
        },

        getEventFailure(state, action) {
            state.getEventsError = action.payload;
            state.getEventsLoading = false;
        },

        deleteEventRequest(state) {
            state.deleteEventLoading = true;
        },

        deleteEventSuccess(state, {payload: eventId}) {
            state.deleteEventLoading = false;
            state.events = state.events.filter(event => event._id !== eventId);
        },

        deleteEventFailure(state, action) {
            state.deleteEventError = action.payload;
            state.deleteEventLoading = false;
        },

        inviteUserRequest(state) {
            state.inviteUserLoading = true;
        },

        inviteUserSuccess(state) {
            state.inviteUserLoading = false;
        },

        inviteUserFailure(state, action) {
            state.inviteUserLoading = false;
            state.inviteUserError = action.payload;
        },

        setModal(state, action) {
            state.setModal = action.payload;
        }
    },
});

export default eventSlice;