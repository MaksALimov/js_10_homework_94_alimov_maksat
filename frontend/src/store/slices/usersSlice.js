import {createSlice} from '@reduxjs/toolkit';

export const initialState = {
    user: null,
    registerError: null,
    registerLoading: null,
    loginError: null,
    loginLoading: null,
    logoutError: null,
    logoutLoading: null,
};

const userSlice = createSlice({
    name: 'users',
    initialState,

    reducers: {
        registerUserRequest(state) {
            state.registerLoading = true;
        },

        registerUserSuccess(state, {payload: userData}) {
            state.user = userData;
            state.registerError = null;
            state.registerLoading = false;
        },

        registerUserFailure(state, {payload: error}) {
            state.registerError = error;
            state.registerLoading = false;
        },

        loginUserRequest(state) {
            state.loginLoading = true;
        },

        loginUserSuccess(state, {payload: userData}) {
            state.loginError = null;
            state.user = userData;
            state.loginLoading = false;
        },

        loginUserFailure(state, {payload: error}) {
            state.loginError = error;
            state.loginLoading = false;
        },

        logoutUserRequest(state) {
            state.user = null;
            state.logoutLoading = true;
        },

        googleLogin() {},

        logoutUserFailure(state, {payload: error}) {
            state.logoutError = error;
            state.logoutLoading = false;
        },

        clearTextFieldsErrors(state) {
            state.registerError = null;
            state.loginError = null;
        }
    },
});

export default userSlice;