import eventSlice from "../slices/eventsSlice";

export const {
    addEventRequest,
    addEventSuccess,
    addEventFailure,
    getEventRequest,
    getEventSuccess,
    getEventFailure,
    deleteEventRequest,
    deleteEventSuccess,
    deleteEventFailure,
    inviteUserRequest,
    inviteUserSuccess,
    inviteUserFailure,
    setModal,
} = eventSlice.actions;