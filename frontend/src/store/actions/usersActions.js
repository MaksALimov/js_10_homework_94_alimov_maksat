import userSlice from "../slices/usersSlice";

export const {
    registerUserRequest,
    registerUserSuccess,
    registerUserFailure,
    loginUserRequest,
    loginUserSuccess,
    loginUserFailure,
    logoutUserRequest,
    logoutUserFailure,
    googleLogin,
    clearTextFieldsErrors,
} = userSlice.actions;