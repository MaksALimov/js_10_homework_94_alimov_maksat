import Layout from "./components/UI/Layout/Layout";
import {Redirect, Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import {useSelector} from "react-redux";
import Events from "./containers/Events/Events";
import AddEvent from "./containers/AddEvent/AddEvent";

const App = () => {
    const user = useSelector(state => state.users.user);

    const Permit = ({isAllowed, redirectTo, ...props}) => {
        return isAllowed ?
            <Route {...props}/> :
            <Redirect to={redirectTo}/>
    };

    return (
        <Layout>
            <Switch>
                <Permit
                    isAllowed={Boolean(user)}
                    path="/"
                    exact
                    component={Events}
                    redirectTo="/register"
                />
                <Permit
                    path="/add-event"
                    isAllowed={Boolean(user)}
                    component={AddEvent}
                    redirectTo="/register"
                />
                <Route path="/register" component={Register}/>
                <Route path="/login" component={Login}/>
            </Switch>
        </Layout>
    )
};

export default App;
